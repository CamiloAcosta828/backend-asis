import { Request, Response } from "express";
import NewsService from "../services/NewsService";

export function getAll(_: any, res: Response) {
    const digimons = NewsService.getAll();
    res.status(200).json(digimons);
}