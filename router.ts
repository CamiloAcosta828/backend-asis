import express from 'express';
import * as NewsController from './src/controllers/NewsController';


export const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World with Typescript!')
})

router.get('/ts', (req, res) => {
    res.send('Typescript es lo máximo!')
})

router.get('/news', NewsController.getAll);
