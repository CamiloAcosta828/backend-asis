import { Category } from "../interfaces/Category";
const db = require('../db/Category.json');

module NewsService {
    export function getAll(): Array<Category> {
        const category: Array<Category> = db;
        return category;
    }
}
export default NewsService;
